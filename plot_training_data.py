#!/usr/bin/env python3

import itertools
import json
import matplotlib.pyplot as plt
import numpy as np
from dtree_synth_gem_stanley import load_partitioned_examples

from gem_stanley_teacher import DTreeGEMStanleyGurobiStabilityTeacher, WHEEL_BASE

C_RED = "#DC3220"
C_BLUE = "#005AB5"

X_LIM = np.inf
X_ARR = np.array([-X_LIM, X_LIM])

Y_LIM = 1.2
# NUM_Y_PARTS = 4
# Y_ARR = np.linspace(-Y_LIM, Y_LIM, NUM_Y_PARTS + 1)
Y_ARR = [-Y_LIM, -0.9, -0.3, 0.0, 0.8, 1.1, Y_LIM]
NUM_Y_PARTS = len(Y_ARR) - 1

YAW_LIM = np.pi / 12
# NUM_YAW_PARTS = 10
# YAW_ARR = np.linspace(-YAW_LIM, YAW_LIM, NUM_YAW_PARTS + 1)
YAW_ARR = [-YAW_LIM, -np.pi/18, -np.pi/36, 0.0, np.pi/24, YAW_LIM]
NUM_YAW_PARTS = len(YAW_ARR) - 1

PARTITION = (X_ARR, Y_ARR, YAW_ARR)

PKL_FILE_PATH = "data/training/800_truths-uniform_partition_4x20-1.2m-pi_12-one_straight_road-2021-10-27-08-49-17.bag.pickle"

NORM_ORD = 2
ULT_BOUND = 1.0


def find_part_grid(part):
    lb, ub = np.asfarray(part).T
    y_id = np.searchsorted(Y_ARR, lb[1])
    assert ub[1] == Y_ARR[y_id+1]

    yaw_id = np.searchsorted(YAW_ARR, lb[2])
    assert ub[2] == YAW_ARR[yaw_id+1]
    return y_id, yaw_id


teacher = DTreeGEMStanleyGurobiStabilityTeacher(
    norm_ord=NORM_ORD, ultimate_bound=ULT_BOUND)
part_to_examples = load_partitioned_examples(
    file_name=PKL_FILE_PATH,
    teacher=teacher, partition=PARTITION
)

# Print statistics about training data points
transposed_table = np.zeros(shape=(NUM_Y_PARTS, NUM_YAW_PARTS), dtype=float)
transposed_table *= np.nan

print("#"*80)
print("Parts with unsafe data points:")
fig, ax = plt.subplots()
for part, dataset in part_to_examples.items():
    i = find_part_grid(part)
    safe_dps, unsafe_dps, nan_dps = dataset.safe_dps, dataset.unsafe_dps, dataset.nan_dps
    num_safe_dps = len(safe_dps)
    num_unsafe_dps = len(unsafe_dps)
    num_nan = len(nan_dps)

    if (num_safe_dps + num_unsafe_dps + num_nan) == 0:
        # No data points in this region part. Skip.
        continue

    # if len(unsafe_dps) > 0:
    #     unsafe_dp_arr = np.asfarray(unsafe_dps).T
    #     ax.scatter(unsafe_dp_arr[1], np.rad2deg(unsafe_dp_arr[2]), c=C_RED, marker='X')
    # if len(nan_dps) > 0:
    #     nan_dp_arr = np.asfarray(nan_dps).T
    #     ax.scatter(nan_dp_arr[1], np.rad2deg(nan_dp_arr[2]), c=C_RED, marker='*')
    # if len(safe_dps) > 0:
    #     safe_dp_arr = np.asfarray(safe_dps).T
    #     ax.scatter(safe_dp_arr[1], np.rad2deg(safe_dp_arr[2]), c=C_BLUE, marker='.', s=2)

    lb, ub = np.asfarray(part).T
    lb[2] = np.rad2deg(lb[2])
    ub[2] = np.rad2deg(ub[2])

    if len(unsafe_dps) > 0 or num_nan > 0:
        print(f"Part Index {i}:", f"y in [{lb[1]:.03}, {ub[1]:.03}] (m);", f"θ in [{lb[2]:.03}, {ub[2]:.03}] (deg);",
              f"# safe: {num_safe_dps:03}", f"# unsafe: {num_unsafe_dps:03}", f"# NaN: {num_nan:03}")

    transposed_table[i] = num_safe_dps / \
        (num_safe_dps + num_unsafe_dps + num_nan)

table = transposed_table.T

if False:
    min_dps = [
        [0.8984375, 0.2275390625],
        [0.9609375, 0.1904296875],
        [0.9619140625, 0.1572265625],
        [0.9736328125, 0.150390625],
        [1.0400390625, 0.1455078125],
        [1.1865234375, 0.1240234375],
        [1.1982421875, 0.03125]]
    for y, yaw in min_dps:
        yaw_deg = np.rad2deg(yaw)
        ax.hlines(y=yaw_deg, xmin=y, xmax=1.25, color='black')
        ax.vlines(x=y, ymin=yaw_deg, ymax=15.5, color='black')

    min_dps = [
        [-1.0009765625, -0.1025390625],
        [-0.91015625, -0.2001953125],
        [-0.5166015625, -0.205078125],
        [-0.4111328125, -0.2470703125],
        [-0.3271484375, -0.26171875]
    ]

    for y, yaw in min_dps:
        yaw_deg = np.rad2deg(yaw)
        ax.hlines(y=yaw_deg, xmin=-1.25, xmax=y, color='black')
        ax.vlines(x=y, ymin=-15.5, ymax=yaw_deg, color='black')

im = ax.pcolormesh(Y_ARR, np.rad2deg(YAW_ARR), 70 * (table == 1.0), cmap="Blues", shading="flat",
                   edgecolors='#005AB5', linewidth=0.5, zorder=0,
                   vmin=0.0, vmax=100.0)

if True:
    for y in range(table.shape[0]):
        for x in range(table.shape[1]):
            ax.text(np.mean(Y_ARR[x:x+2]), np.rad2deg(np.mean(YAW_ARR[y:y+2])), f'{table[y, x]:.2f}',
                horizontalalignment='center',
                verticalalignment='center',
            )

if False:
    if True:
        JSON_PATH = "concat1_0-all/out/dtree_synth.4x10.out.json"
        # JSON_PATH = "exp_results/gem-stability-concat-ult_bound=0.0/dtree_synth.4x10.out.json"
        with open(JSON_PATH, "r") as f:
            partitions_w_results = json.load(f)
    else:
        with open("diff1_0-safe/out/dtree_synth.4x10.out.json", "r") as f:
            safe_partitions_w_results = json.load(f)
        with open("diff1_0-unsafe/out/dtree_synth.4x10.out.json") as f:
            unsafe_partitions_w_results = json.load(f)
            # unsafe_partitions_w_results = []
        partitions_w_results = itertools.chain(safe_partitions_w_results, unsafe_partitions_w_results)

    list_found_grids = []
    for res_dict in partitions_w_results:
        if res_dict['status'] == "found":
            part = tuple(tuple(pair) for pair in res_dict['part'])
            grid = find_part_grid(part)
            list_found_grids.append(grid)

    for x, y in list_found_grids:
        ax.text(np.mean(Y_ARR[x:x+2]), np.rad2deg(np.mean(YAW_ARR[y:y+2])), '\u2714',
                horizontalalignment='center',
                verticalalignment='center',
            )

# Plot arrows representing car heading and distance to center
vel = WHEEL_BASE * 0.75
y, yaw = np.meshgrid(Y_ARR[0::2], YAW_ARR[0::2])
u, v = vel*np.sin(yaw), vel*np.cos(yaw)
ax.quiver(y, np.rad2deg(yaw), u, v, angles='uv', scale_units='x', scale=1.0)

ax.axvline(x=0.0, color=C_RED, linestyle='--')
ax.axvline(x=-2.0, color=C_RED, linestyle='-')
ax.axvline(x=2.0, color=C_RED, linestyle='-')

ax.set_xlim((-2.2, 2.2))
ax.set_ylim((-16, 21))
ax.set_yticks(ticks=list(range(-15, 18, 3)))
ax.set_aspect(0.25)

fig.set_size_inches(4, 6.875)
ax.set_xlabel(xlabel="relative distance (meter)", loc='center')
ax.set_ylabel(ylabel=u"relative heading (degree)", loc='center')
fig.tight_layout()
fig.savefig("result-2D-relaxed.png")
exit()

fig.suptitle("Regions with learned contracts\n"
             "4D feature space, strict invariant\n"
             "Range of speed: [2.5, 3.0] (m/s)")
fig.tight_layout()
fig.savefig("result-4D-relaxed.png")
