import matplotlib.pyplot as plt
import numpy as np
import json
import sys
from dtree_synth_gem_stanley import load_partitioned_examples

from gem_stanley_teacher import DTreeGEMStanleyGurobiStabilityTeacher

X_LIM = np.inf
X_ARR = np.array([-X_LIM, X_LIM])

Y_LIM = 1.2
NUM_Y_PARTS = 4
Y_ARR = np.linspace(-Y_LIM, Y_LIM, NUM_Y_PARTS + 1)

YAW_LIM = np.pi / 12
NUM_YAW_PARTS = 10  
YAW_ARR = np.linspace(-YAW_LIM, YAW_LIM, NUM_YAW_PARTS + 1)

PARTITION = (X_ARR, Y_ARR, YAW_ARR)

NORM_ORD = 2
ULT_BOUND = 1.0

PKL_FILE_PATH = "data/training/800_truths-uniform_partition_4x20-1.2m-pi_12-one_straight_road-2021-10-27-08-49-17.bag.pickle"
JSON_RES_FILE = "exp_results/gem-stability-concat-ult_bound=1.0/dtree_synth.4x10.out.json"
JSON_RES_FILE = "exp_results/gem-stability-diff-ult_bound=1.0/dtree_synth.4x10.out.json"

def find_part_grid(part):
    lb, ub = np.asfarray(part).T
    y_id = np.searchsorted(Y_ARR, lb[1])
    assert ub[1] == Y_ARR[y_id+1]

    yaw_id = np.searchsorted(YAW_ARR, lb[2])
    assert ub[2] == YAW_ARR[yaw_id+1]
    return y_id, yaw_id


def get_parts(file):
    partitions_w_results=[]
    with open(file) as f:
        partitions_w_results = json.load(f)
    return partitions_w_results
    # rets = []
    # for part_w_res in partitions_w_results:
    #     part = part_w_res["part"]
    #     res = part_w_res["status"]
    #     if res != "found":
    #          rets.append(part)   
    # return rets
def convert_list_to_tuple(lst):
    ltup = []
    for l in lst:
        ltup.append(tuple(l))

    return tuple(ltup)

teacher = DTreeGEMStanleyGurobiStabilityTeacher(norm_ord=NORM_ORD, ultimate_bound=ULT_BOUND)
part_to_examples = load_partitioned_examples(
        file_name=PKL_FILE_PATH,
        teacher=teacher, partition=PARTITION
    )
idx1 = 0
data_list = []
for part, dataset in part_to_examples.items():
    data_list.append(part)
    idx1 += 1
    #break

transposed_table = np.zeros(shape=(NUM_Y_PARTS, NUM_YAW_PARTS), dtype=float)
transposed_table *= np.nan
total_used = 0

partitions = []
for item in get_parts(JSON_RES_FILE):
    print(type(item))
    #print(item)
    print(type(item["part"]))
    print(item["part"])
    part_tup = convert_list_to_tuple(item["part"])

    i = find_part_grid(part_tup)
    print(i) 
    lb, ub = np.asfarray(part_tup).T
    lb[2]= np.rad2deg(lb[2])
    ub[2]= np.rad2deg(ub[2])
    print(item["status"])
    transposed_table[i] = 1.0 if item["status"] == "found" else 0.0

table = transposed_table.T

print(table)

fig, ax = plt.subplots()
im = ax.pcolormesh(Y_ARR, np.rad2deg(YAW_ARR), 100 * table, cmap="Greens", shading="flat",
                    vmin=0.0, vmax=100.0)

if __debug__:
    for y in range(table.shape[0]):
        for x in range(table.shape[1]):
            plt.text(np.mean(Y_ARR[x:x+2]), np.rad2deg(np.mean(YAW_ARR[y:y+2])), f'{table[y, x]:.3f}',
                horizontalalignment='center',
                verticalalignment='center',
            )

fig.colorbar(im, ax=ax)
plt.savefig("old_results.png")

# idx = 0
# part_res_list = []
# for part_w_res in data:
#     part_res_list.append(part_w_res)
#     idx += 1
#     # break

# assert len(data_list) == len(part_res_list)

# for i in range(0,len(data_list)):
#     part = data_list[i]
#     part_res = part_res_list[i]["part"]
#     #part_copy = tuple()
#     part_copy = part[0]+ part[1]+ tuple(np.rad2deg(part[2]))
#     part_res_copy = tuple(part_res[0]) + tuple(part_res[1])+ tuple(np.rad2deg(part_res[2]))
#     print(part_copy)

#     #print(part_res)
#     if part_res_list[i]["status"] != "found":
#         print(part_res_copy)
#         print(part_res_list[i]["status"])
#     #print(part_res_list[i]["status"])

#     print("-----------------")
#     #break

