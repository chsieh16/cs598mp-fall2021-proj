import abc
import itertools
from typing import Dict, List, Sequence, Tuple

import dreal
import numpy as np
import z3

from teacher_base import TeacherBase


class DRealTeacherBase(TeacherBase):
    @staticmethod
    def affine_trans_exprs(state, coeff: np.ndarray, intercept=None):
        if intercept is None:
            intercept = np.zeros(coeff.shape[0])
        assert (len(intercept), len(state)) == coeff.shape
        return [
            b_i + sum([col_j*var_j for col_j, var_j in zip(row_i, state)])
            for row_i, b_i in zip(coeff, intercept)
        ]

    def __init__(self, name: str,
                 state_dim: int, perc_dim: int, ctrl_dim: int, norm_ord,
                 delta: float = 0.001) -> None:
        self._norm_ord = norm_ord
        self._delta = delta

        # Old state variables
        self._old_state = [dreal.Variable(f"x[{i}]", dreal.Variable.Real) for i in range(state_dim)]
        # New state variables
        self._new_state = [dreal.Variable(f"x'[{i}]", dreal.Variable.Real) for i in range(state_dim)]
        # Perception variables
        self._percept = [dreal.Variable(f"z[{i}]", dreal.Variable.Real) for i in range(perc_dim)]
        # Control variables
        self._control = [dreal.Variable(f"u[{i}]", dreal.Variable.Real) for i in range(ctrl_dim)]

        self._var_bounds = {
            var: (-np.inf, np.inf)
            for var in itertools.chain(self._old_state, self._percept, self._control)
        }  # type: Dict[dreal.Variable, Tuple[float, float]]
        self._not_inv_cons = []  # type: List
        self._models = []  # type: List

        self._add_system()
        self._add_unsafe()

    @abc.abstractmethod
    def _add_system(self) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def _add_unsafe(self) -> None:
        raise NotImplementedError

    @property
    def state_dim(self) -> int:
        return len(self._old_state)

    @property
    def perc_dim(self) -> int:
        return len(self._percept)

    @property
    def ctrl_dim(self) -> int:
        return len(self._control)

    def _gen_cand_pred(self, candidate):
        raise NotImplementedError(f"TODO convert {candidate} to dReal formula")

    def _set_var_bound(self, smt_vars: List[dreal.Variable], lb: Sequence[float], ub: Sequence[float]) -> None:
        assert len(lb) == len(smt_vars)
        assert len(ub) == len(smt_vars)
        for var_i, lb_i, ub_i in zip(smt_vars, lb, ub):
            self._var_bounds[var_i] = (lb_i, ub_i)

    def set_old_state_bound(self, lb: Sequence[float], ub: Sequence[float]) -> None:
        self._set_var_bound(self._old_state, lb, ub)

    def check(self, candidate) -> z3.CheckSatResult:
        bound_preds = [
            dreal.And(lb_i <= var_i, var_i <= ub_i)
            for var_i, (lb_i, ub_i) in self._var_bounds.items()
        ]
        cand_pred = self._gen_cand_pred(candidate)
        query = dreal.And(*bound_preds, cand_pred, *self._not_inv_cons)
        res = dreal.CheckSatisfiability(query, self._delta)

        self._models.clear()
        if res:
            self._models.append(
                tuple(res[x_i] for x_i in self._old_state) + tuple(res[z_i] for z_i in self._percept)
            )
            return z3.sat
        else:
            return z3.unsat

    def dump_system_encoding(self, basename: str = "") -> None:
        print([
            dreal.And(lb_i <= var_i, var_i <= ub_i)
            for var_i, (lb_i, ub_i) in self._var_bounds.items()
        ])
        print(self._not_inv_cons)

    def model(self) -> Sequence[Tuple]:
        return self._models

    def reason_unknown(self) -> str:
        raise NotImplementedError

