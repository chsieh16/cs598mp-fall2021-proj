import pickle

import numpy as np
import matplotlib.pyplot as plt

# Constants for Stanley controller for GEM
WHEEL_BASE = 1.75  # m
K_P = 0.45
CYCLE_TIME = 0.05  # second
FORWARD_VEL = 2.8  # m/s
STEERING_LIM = 0.61  # rad


def g(cte, phi):
    error = phi + np.arctan(K_P*cte/FORWARD_VEL)
    steer = np.clip(error, -STEERING_LIM, STEERING_LIM)
    return (steer,)


def f(x, y, theta, steer):
    new_x = x + FORWARD_VEL*np.cos(theta+steer)*CYCLE_TIME
    new_y = y + FORWARD_VEL*np.sin(theta+steer)*CYCLE_TIME
    new_theta = theta + FORWARD_VEL*np.sin(steer)/WHEEL_BASE*CYCLE_TIME
    return new_x, new_y, new_theta


def v(x, y, theta) -> float:
    return np.linalg.norm([y, theta], ord=2)


def in_circle(x, y, theta, d, phi) -> bool:
    a_mat = np.array([[0.0, -1.0, 0.0],
                      [0.0, 0.0, -1.0]])
    b_vec = np.zeros(2)
    state_vec = np.array([x, y, theta])
    perc_vec = np.array([d, phi])

    diff_vec = perc_vec - (a_mat @ state_vec + b_vec)
    return np.linalg.norm(diff_vec, ord=2) <= np.sqrt(0.03)


def pred(sample) -> bool:
    x, y, theta, d, phi = sample
    return v(*f(x, y, theta, *g(d, phi))) <= v(x, y, theta) \
        and in_circle(x, y, theta, d, phi)


in_file_name = "collect_images_2021-11-22-17-59-46.cs598.pickle"
out_file_name = "collect_images_2021-11-22-17-59-46.cs598.filtered.pickle"

with open(in_file_name, "rb") as in_file:
    pkl_data = pickle.load(in_file)
    truth_samples_seq = pkl_data["truth_samples"]


filtered_truth_samples = []

for truth, samples in truth_samples_seq:
    cte, phi = truth
    filtered_samples = [s for s in samples if pred(s)]
    if abs(len(samples) - len(filtered_samples)) < 200 and \
            abs(cte - 0.967823751) <= 0.001 and abs(phi - -0.211146388) <= 0.001:
        print("#Original:", len(samples), "#Filtered:", len(filtered_samples))
        filtered_truth_samples.append((truth, filtered_samples))
    else:
        # print("Skip partition. #Original:", len(samples), "#Filtered:",  len(filtered_samples))
        continue

center = filtered_truth_samples[0][0]
print("Representative point:", center)
x_arr, y_arr = np.array(filtered_truth_samples[0][1])[:, 3:5].T
ax = plt.gca()
ax.set_aspect("equal")
ax.add_patch(plt.Circle(center, np.sqrt(0.03), color='g', alpha=0.3))
plt.scatter(x_arr, y_arr)
plt.plot(*center, 'go')
plt.savefig("temp.png")

with open(out_file_name, "wb") as out_file:
    pkl_data["truth_samples"] = filtered_truth_samples
    pickle.dump(pkl_data, out_file)
