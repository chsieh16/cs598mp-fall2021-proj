#!/usr/bin/env python3

import itertools
import json
from typing import Dict, Hashable, Iterable, Literal, Tuple

import numpy as np
from dtree_synth import DataSet, search_part, synth_dtree_per_part
from dtree_learner import DTreeLearner as DTreeLearner
from agbot_stanley_teacher import DTreeAgBotStanleyGurobiTeacher
from teacher_base import TeacherBase


def load_examples_from_numpy_array(
        data_5d: Iterable[Tuple[float, ...]],
        teacher: DTreeAgBotStanleyGurobiTeacher,
        partition) -> Dict[Hashable, DataSet]:
    # x_arr[:-1], except the last one, x_arr[1:] except the first one
    bound_list = list(list(zip(x_arr[:-1], x_arr[1:])) for x_arr in partition)
    ret = {part: DataSet() for part in itertools.product(*bound_list)}

    num_excl_samples = 0
    for dpoint in data_5d:
        vehicle_state = dpoint[0:teacher.state_dim]
        part = search_part(partition, vehicle_state)
        if part is None:
            num_excl_samples += 1
            continue
        if np.any(np.isnan(dpoint)):
            ret[part].nan_dps.append(dpoint)
        elif teacher.is_safe_state(dpoint):
            ret[part].safe_dps.append(dpoint)
        else:
            ret[part].unsafe_dps.append(dpoint)
    print("# samples not in any selected parts:", num_excl_samples)
    return ret


def main(dom: Literal["concat", "diff"], ult_bound: float):
    NPY_FILE_PATH = "data/training/400_truths-uniform_partition_20x20-0.228m-pi_6-agbot-2021-10-29-01-37-44.npy"

    print("Loading examples from .npy")
    data_5d_structured_arr = np.load(NPY_FILE_PATH)
    data_5d = data_5d_structured_arr[['x', 'y', 'yaw', 'cte', 'psi']].tolist()

    # Partitions on prestate

    X_LIM = np.inf
    X_ARR = np.array([-X_LIM, X_LIM])

    PRE_Y_LIM = 0.228
    NUM_Y_PARTS = 5
    Y_ARR = np.linspace(-PRE_Y_LIM, PRE_Y_LIM, NUM_Y_PARTS + 1)

    PRE_YAW_LIM = np.pi / 6
    NUM_YAW_PARTS = 5
    YAW_ARR = np.linspace(-PRE_YAW_LIM, PRE_YAW_LIM, NUM_YAW_PARTS + 1)

    PARTITION = (X_ARR, Y_ARR, YAW_ARR)
    NUM_MAX_ITER = 150
    MAX_TIME_PER_PART = 3600.0  # seconds
    FEATURE_DOMAIN = dom
    ULT_BOUND = ult_bound
    NORM_ORD = 2

    teacher = DTreeAgBotStanleyGurobiTeacher(norm_ord=NORM_ORD, ultimate_bound=ULT_BOUND)
    part_to_examples = load_examples_from_numpy_array(data_5d, teacher, PARTITION)

    # Print statistics about training data points
    print("#"*80)
    print("Parts with unsafe data points:")
    for i, (part, dataset) in enumerate(part_to_examples.items()):
        safe_dps, unsafe_dps, nan_dps = dataset.safe_dps, dataset.unsafe_dps, dataset.nan_dps
        num_nan = len(nan_dps)
        lb, ub = np.asfarray(part).T
        lb[2] = np.rad2deg(lb[2])
        ub[2] = np.rad2deg(ub[2])

        if len(unsafe_dps) > 0 or len(nan_dps) > 0:
            print(f"Part Index {i}:", f"y in [{lb[1]:.03}, {ub[1]:.03}] (m);", f"θ in [{lb[2]:.03}, {ub[2]:.03}] (deg);",
                  f"# safe: {len(safe_dps)}", f"# unsafe: {len(unsafe_dps)}", f"# NaN: {num_nan}")

    def teacher_builder() -> DTreeAgBotStanleyGurobiTeacher:
        return DTreeAgBotStanleyGurobiTeacher(norm_ord=NORM_ORD, ultimate_bound=ULT_BOUND)

    def learner_builder(teacher: TeacherBase) -> DTreeLearner:
        learner = DTreeLearner(state_dim=teacher.state_dim,
                               perc_dim=teacher.perc_dim, timeout=20000)
        learner.set_grammar([(DTreeAgBotStanleyGurobiTeacher.PERC_GT, np.zeros(2))], FEATURE_DOMAIN)
        return learner

    result = synth_dtree_per_part(
        part_to_examples,
        teacher_builder,
        learner_builder,
        num_max_iter=NUM_MAX_ITER,
        max_time_per_part=MAX_TIME_PER_PART,
        ult_bound=ULT_BOUND,
        feature_domain=FEATURE_DOMAIN
    )

    with open(f"out/dtree_synth.{NUM_Y_PARTS}x{NUM_YAW_PARTS}.out.json", "w") as f:
        json.dump(result, f)


if __name__ == "__main__":
    # NOTE To ensure safety (Stay in corn row),
    # ult_bound should be smaller than 33/32 = 1.03125

    from dtree_synth_gem_stanley import clean_out_dir

    # main("diff", 0.0)
    # clean_out_dir("out-agbot-stability-diff-ult_bound=0.0")

    # main("diff", 1.0)
    # clean_out_dir("out-agbot-stability-diff-ult_bound=1.0")

    # main("concat", 0.0)
    # clean_out_dir("out-agbot-stability-concat-ult_bound=0.0")

    main("concat", 1.0)
    clean_out_dir("out-agbot-stability-concat-ult_bound=1.0")
