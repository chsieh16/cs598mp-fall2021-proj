#!/usr/bin/env python3
import dataclasses
import itertools
import json
import pickle
from timeit import default_timer as timer
from typing import Hashable, List, Mapping, Sequence, Tuple
import warnings

import numpy as np
from sklearn.linear_model import LinearRegression
import z3

from compute_aap.gem_stanley_min_dist import PERC_GT, GEMStanleyV2DNonInc


@dataclasses.dataclass
class DataSet:
    safe_dps: List[Tuple[float, ...]] = dataclasses.field(default_factory=list)
    unsafe_dps: List[Tuple[float, ...]] = dataclasses.field(
        default_factory=list)
    num_nan_dps: int = 0


def search_part(partition, state):
    assert len(partition) == len(state)
    bounds = []
    for sorted_list, v in zip(partition, state):
        i = np.searchsorted(sorted_list, v)
        if i == 0 or i == len(sorted_list):
            return None
        bounds.append((sorted_list[i-1], sorted_list[i]))
    return tuple(bounds)


def linear_regression(
        samples_seq: Sequence[Tuple[float, ...]],
        fit_intercept: bool) -> Tuple[np.ndarray, np.ndarray]:
    # Filter samples with NaN and inf
    samples_seq = [s for s in samples_seq if np.all(np.isfinite(s))]
    if len(samples_seq) == 0:
        warnings.warn(
            "No samples for linear regression. Return identity transformation.")
        return np.identity(2), np.zeros(2)
    samples_arr = np.asfarray(samples_seq)

    # TODO get and check state dimension and percept dimension
    assert samples_arr.shape[1] == 5
    x_arr = (PERC_GT @ samples_arr[:, 0:3].T).T
    y_arr = samples_arr[:, 3:5]
    assert len(x_arr) == len(y_arr)

    regressor = LinearRegression(fit_intercept=fit_intercept, copy_X=False)
    regressor.fit(x_arr, y_arr)
    assert regressor.coef_.shape == (2, 2)
    if not fit_intercept:
        intercept = np.zeros(2)
    else:
        assert regressor.intercept_.shape == (2,), str(regressor.intercept_)
        intercept = regressor.intercept_
    return regressor.coef_, intercept


def load_partitioned_examples(file_name: str, min_dist: GEMStanleyV2DNonInc, partition) \
        -> Mapping[Hashable, DataSet]:
    print("Loading examples")
    with open(file_name, "rb") as pickle_file_io:
        pkl_data = pickle.load(pickle_file_io)

    truth_samples_seq = pkl_data["truth_samples"]

    bound_list = list(list(zip(x_arr[:-1], x_arr[1:])) for x_arr in partition)
    ret = {part: DataSet() for part in itertools.product(*bound_list)}
    # Convert from sampled states and percepts to positive and negative examples for learning
    num_excl_samples = 0
    for _, ss in truth_samples_seq:
        for s in ss:
            state = s[0:min_dist.state_dim]
            part = search_part(partition, state)
            if part is None:
                num_excl_samples += 1
                continue
            if np.any(np.isnan(s)):
                ret[part].num_nan_dps += 1
            # TODO note, isin't variable _ being passed to is_positive.
            # isint _ ground truth for each s in ss?
            elif min_dist.is_safe_state(s):
                ret[part].safe_dps.append(s)
            else:
                ret[part].unsafe_dps.append(s)
    print("# samples not in any selected parts:", num_excl_samples)
    return ret


def main(bnd):
    X_LIM = np.inf
    X_ARR = np.array([-X_LIM, X_LIM])

    Y_LIM = 1.2
    NUM_Y_PARTS = 4
    Y_ARR = np.linspace(-Y_LIM, Y_LIM, NUM_Y_PARTS + 1)

    YAW_LIM = np.pi / 12
    NUM_YAW_PARTS = 10
    YAW_ARR = np.linspace(-YAW_LIM, YAW_LIM, NUM_YAW_PARTS + 1)

    PARTITION = (X_ARR, Y_ARR, YAW_ARR)

    PKL_FILE_PATH = "data/800_truths-uniform_partition_4x20-1.2m-pi_12-one_straight_road-2021-10-27-08-49-17.bag.pickle"

    min_dist = GEMStanleyV2DNonInc(ult_bound=bnd)

    part_to_examples = load_partitioned_examples(
        file_name=PKL_FILE_PATH,
        min_dist=min_dist, partition=PARTITION
    )

    for i, (part, dataset) in enumerate(part_to_examples.items()):
        safe_dps, unsafe_dps, num_nan = dataset.safe_dps, dataset.unsafe_dps, dataset.num_nan_dps

        lb, ub = np.asfarray(part).T
        lb[2] = np.rad2deg(lb[2])
        ub[2] = np.rad2deg(ub[2])

        if len(unsafe_dps) > 0:
            print(f"Part Index {i}:", f"y in [{lb[1]:.03}, {ub[1]:.03}] (m);", f"θ in [{lb[2]:.03}, {ub[2]:.03}] (deg);",
                  f"# safe: {len(safe_dps)}", f"# unsafe: {len(unsafe_dps):03}", f"# NaN: {num_nan}")

    res = []
    for part, dataset in part_to_examples.items():
        t_start = timer()
        a_mat, b_vec = linear_regression(dataset.safe_dps, True)

        t_end_learn = timer()

        min_dist = GEMStanleyV2DNonInc(coeff=a_mat, intercept=b_vec, ult_bound=bnd)
        lb, ub = np.asfarray(part).T
        min_dist.set_init_state_bound(lb=lb, ub=ub)
        min_dist.set_pre_state_bound(lb=lb, ub=ub)
        r = min_dist.compute_min_dist()
        t_end = timer()

        # r == 0 is considered not found
        status = "found" if (not np.isnan(r)) and r > 0 else "not found"

        if np.isposinf(r):
            z3_expr = z3.BoolVal(True)
        else:
            state_vars = z3.Reals([f"x_{i}" for i in range(min_dist.state_dim)])
            perc_vars = z3.Reals([f"z_{i}" for i in range(min_dist.perc_dim)])

            z3_expr = sum(expr**2 for expr in (perc_vars - a_mat @ PERC_GT @ state_vars - b_vec)) <= r**2

        z3_expr = z3.simplify(z3_expr)

        entry = {
            "part": part,
            "ultimate_bound": bnd,
            "status": status,
            "result" : {"formula": z3_expr.sexpr(), "smtlib": z3_expr.serialize()},
            "learner time": t_end_learn - t_start,
            "teacher time": t_end - t_end_learn
        }
        res.append(entry)

    with open(f"aap.{NUM_Y_PARTS}x{NUM_YAW_PARTS}-ult_bound{bnd}.out.json", "w") as f:
        json.dump(res, f)
    

if __name__ == "__main__":
    main(1.0)
