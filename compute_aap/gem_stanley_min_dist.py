import abc
from typing import Literal, Optional

import gurobipy as gp
import numpy as np

from compute_aap.min_dist_base import GurobiMinDistBase

# Constants for Stanley controller for GEM
WHEEL_BASE = 1.75  # m
K_P = 0.45
CYCLE_TIME = 0.05  # second
FORWARD_VEL = 2.8  # m/s
STEERING_LIM = 0.61  # rad

# Limits on unconstrained variables to avoid overflow and angle normalization
ANG_LIM = np.pi / 2  # radian
CTE_LIM = 2.0  # meter

# Specifications
INIT_Y_LIM = 1.2
INIT_YAW_LIM = np.pi / 12
SAFE_Y_LIM = 1.6
SAFE_YAW_LIM = ANG_LIM

# Bounds for intermediate variables.
# These are not necessary but can improve efficiency
K_CTE_V_LIM = K_P * CTE_LIM / FORWARD_VEL
ATAN_K_CTE_V_LIM = np.arctan(K_CTE_V_LIM)
RAW_ANG_ERR_LIM = (ANG_LIM + ATAN_K_CTE_V_LIM)

NEW_K_CTE_V_LIM = K_CTE_V_LIM + FORWARD_VEL * CYCLE_TIME * 1.0
NEW_ATAN_K_CTE_V_LIM = np.arctan(NEW_K_CTE_V_LIM)
NEW_RAW_ANG_ERR_LIM = ANG_LIM + FORWARD_VEL * CYCLE_TIME

# Ideal perception as a linear transform from state to ground truth percept
PERC_GT = np.array([[0., -1., 0.],
                    [0., 0., -1.]], float)


class GEMStanleyMinDistBase(GurobiMinDistBase):
    """
        Compute minimum distance using a positive definite matrix P induced
        elliptic norm.
        We define P using Cholesky decomposition, P = U^T U, where U is a
        non-negative upper triangular matrix, and equivalently |x|_P = |Ux|
    """
    _NORM_ORD: Literal[1, 2, "inf"] = 2

    def __init__(self,
                 coeff: Optional[np.ndarray] = None,
                 intercept: Optional[np.ndarray] = None) -> None:
        super().__init__(name="gem_stanley", state_dim=3, perc_dim=2, ctrl_dim=1)

        if self._NORM_ORD == 2:
            self._init_level_bound_model.setParam("NonConvex", 2)
            self._safe_level_bound_model.setParam("NonConvex", 2)
            self._inv_model.setParam("NonConvex", 2)

        self._coeff = coeff if coeff is not None else np.identity(self.perc_dim)
        self._intercept = intercept if intercept is not None else np.zeros(self.perc_dim)

        self._add_init_level_bound()
        self._add_safe_level_bound()

        # Encode invariant checking
        self._add_system()
        self._add_inv_check()

    @abc.abstractmethod
    def _add_init_level_bound(self) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def _add_safe_level_bound(self) -> None:
        raise NotImplementedError

    def _add_system(self) -> None:
        # Bounds on all domains
        self._old_state.lb = (-np.inf, -CTE_LIM, -ANG_LIM)
        self._old_state.ub = (np.inf, CTE_LIM, ANG_LIM)
        self._percept.lb = (-CTE_LIM, -ANG_LIM)
        self._percept.ub = (CTE_LIM, ANG_LIM)
        self._control.lb = (-STEERING_LIM,)
        self._control.ub = (STEERING_LIM,)

        # Variable Aliases
        m = self._inv_model
        old_x, old_y, old_yaw = self._old_state.tolist()
        new_x, new_y, new_yaw = self._new_state.tolist()
        # FIXME the state x, y is using the frame of rear wheel center,
        # This is possibly a bug.
        cte, phi = self._percept.tolist()
        steering, = self._control.tolist()

        # Controller
        K_cte_V = m.addVar(name="K*d/Vf", lb=-K_CTE_V_LIM, ub=K_CTE_V_LIM)
        m.addConstr(K_cte_V == K_P * cte / FORWARD_VEL)
        atan_K_cte_V = m.addVar(name="atan(K*d/Vf)",
                                lb=-ATAN_K_CTE_V_LIM, ub=ATAN_K_CTE_V_LIM)
        m.addGenConstrTan(xvar=atan_K_cte_V, yvar=K_cte_V)
        # Clip steering angle
        error = m.addVar(name="(φ+atan(K*d/Vf))",
                         lb=-RAW_ANG_ERR_LIM, ub=RAW_ANG_ERR_LIM)
        m.addConstr(error == phi + atan_K_cte_V)

        xpts = [-RAW_ANG_ERR_LIM, -STEERING_LIM, STEERING_LIM, RAW_ANG_ERR_LIM]
        ypts = [-STEERING_LIM, -STEERING_LIM, STEERING_LIM, STEERING_LIM]
        m.addGenConstrPWL(name="clip", xvar=error, yvar=steering,
                          xpts=xpts, ypts=ypts)

        # Dynamics
        yaw_steer = m.addVar(name="θ+δ",
                             lb=-(ANG_LIM + STEERING_LIM),
                             ub=(ANG_LIM + STEERING_LIM))
        m.addConstr(yaw_steer == (old_yaw + steering))

        cos_yaw_steer = m.addVar(name="cos(θ+δ)", **self.TRIGVAR)
        m.addGenConstrCos(yaw_steer, cos_yaw_steer)
        sin_yaw_steer = m.addVar(name="sin(θ+δ)", **self.TRIGVAR)
        m.addGenConstrSin(yaw_steer, sin_yaw_steer)
        sin_steer = m.addVar(name="sinδ", **self.TRIGVAR)
        m.addGenConstrSin(steering, sin_steer)

        m.addConstr(new_x == old_x + FORWARD_VEL * CYCLE_TIME * cos_yaw_steer)
        m.addConstr(new_y == old_y + FORWARD_VEL * CYCLE_TIME * sin_yaw_steer)
        m.addConstr(new_yaw ==
                    old_yaw + sin_steer * FORWARD_VEL * CYCLE_TIME / WHEEL_BASE)

    @abc.abstractmethod
    def _add_inv_check(self) -> None:
        raise NotImplementedError


class GEMStanleyV2DBase(GEMStanleyMinDistBase):
    def __init__(self,
                 coeff: Optional[np.ndarray] = None,
                 intercept: Optional[np.ndarray] = None) -> None:
        super(GEMStanleyV2DBase, self).__init__(
            coeff=coeff, intercept=intercept)

    def _add_init_level_bound(self) -> None:
        # Variable Aliases
        m = self._init_level_bound_model
        self._init_state.lb = (-np.inf, -CTE_LIM, -ANG_LIM)
        self._init_state.ub = (np.inf, CTE_LIM, ANG_LIM)

        normed_init_truth = m.addMVar(shape=(self.perc_dim,), name="m(x)", **self.FREEVAR)
        m.addConstr(normed_init_truth == PERC_GT @ self._init_state)

        norm_var = m.addVar(name="|m(x)|", **self.NNEGVAR)
        m.addConstr(norm_var == gp.norm(normed_init_truth, float(self._NORM_ORD)))
        m.setObjective(norm_var, gp.GRB.MAXIMIZE)

    def _add_safe_level_bound(self) -> None:
        self._safe_state.lb = (-np.inf, -CTE_LIM, -ANG_LIM)
        self._safe_state.ub = (np.inf, CTE_LIM, ANG_LIM)

        # Variable Aliases
        m = self._safe_level_bound_model
        x, y, yaw = self._safe_state.tolist()

        safe_perc = m.addMVar(shape=(self.perc_dim,), name="m(x)", **self.FREEVAR)
        m.addConstr(safe_perc == PERC_GT @ self._safe_state)

        ind_unsafe_y = m.addVar(vtype=gp.GRB.BINARY)
        abs_y = m.addVar(name="|y|", **self.NNEGVAR)
        m.addConstr(abs_y == gp.abs_(y))
        m.addConstr((ind_unsafe_y == 1) >> (abs_y >= SAFE_Y_LIM))

        ind_unsafe_yaw = m.addVar(vtype=gp.GRB.BINARY)
        abs_yaw = m.addVar(name="|θ|", **self.NNEGVAR)
        m.addConstr(abs_yaw == gp.abs_(yaw))
        m.addConstr((ind_unsafe_yaw == 1) >> (abs_yaw >= SAFE_YAW_LIM))

        ind = m.addVar(vtype=gp.GRB.BINARY)
        m.addConstr(ind == gp.or_(ind_unsafe_y, ind_unsafe_yaw))
        m.addConstr(ind == 1)

        norm_var = m.addVar(name="|m(x)|", **self.NNEGVAR)
        m.addConstr(norm_var == gp.norm(safe_perc, float(self._NORM_ORD)))
        m.setObjective(norm_var, gp.GRB.MINIMIZE)

    @abc.abstractmethod
    def _add_inv_check(self) -> None:
        raise NotImplementedError


class GEMStanleyV2DNonInc(GEMStanleyV2DBase):
    def __init__(self,
                 coeff: Optional[np.ndarray] = None,
                 intercept: Optional[np.ndarray] = None,
                 ult_bound: float = 0.0) -> None:
        assert ult_bound >= 0.0
        self._ult_bound = ult_bound
        super(GEMStanleyV2DNonInc, self).__init__(
            coeff=coeff, intercept=intercept)

    def is_safe_state(self, ex) -> bool:
        assert len(ex) == self.state_dim + self.perc_dim

        def g(perc):
            cte, psi = perc
            error = psi + np.arctan(K_P*cte/FORWARD_VEL)
            steer = np.clip(error, -STEERING_LIM, STEERING_LIM)
            return (steer,)

        def f(state, ctrl):
            x, y, theta = state
            steer, = ctrl
            new_x = x + FORWARD_VEL*np.cos(theta+steer)*CYCLE_TIME
            new_y = y + FORWARD_VEL*np.sin(theta+steer)*CYCLE_TIME
            new_theta = theta + FORWARD_VEL*np.sin(steer)/WHEEL_BASE*CYCLE_TIME
            return new_x, new_y, new_theta

        def m_star(state):
            return PERC_GT @ state

        def v(gt_perc) -> float:
            return float(np.linalg.norm(gt_perc, ord=float(self._NORM_ORD)))

        def spec(state, perc) -> bool:
            v_old = v(m_star(state))
            v_new = v(m_star(f(state, g(perc))))
            return v_new <= max(v_old, self._ult_bound)
        return spec(ex[0:self.state_dim],
                    ex[self.state_dim: self.state_dim+self.perc_dim])

    def _add_inv_check(self) -> None:
        assert PERC_GT.shape == (self.perc_dim, self.state_dim)

        # Variable Aliases
        m = self._inv_model
        # Nondeterministic Perception
        m.addConstr(self._normed_percept_diff ==
                    self._percept
                    - self._coeff @ PERC_GT @ self._old_state
                    - self._intercept)

        # Add objective
        norm_var = m.addVar(name="|z-(Am(x)+b)|", **self.NNEGVAR)
        m.addConstr(norm_var == gp.norm(self._normed_percept_diff, float(self._NORM_ORD)))
        m.setObjective(norm_var, gp.GRB.MINIMIZE)

        normed_old_truth = m.addMVar(shape=(self.perc_dim,), name="m(x)", **self.FREEVAR)
        m.addConstr(normed_old_truth == PERC_GT @ self._old_state)
        normed_new_truth = m.addMVar(shape=(self.perc_dim,), name="m(x')", **self.FREEVAR)
        m.addConstr(normed_new_truth == PERC_GT @ self._new_state)

        old_lya_val = m.addVar(name="|m(x)|", **self.NNEGVAR)
        m.addConstr(old_lya_val == gp.norm(normed_old_truth, float(self._NORM_ORD)))

        if self._ult_bound > 0.0:
            bound_new_lya_val = m.addVar(name=f"max(|m(x)|,{self._ult_bound})", **self.NNEGVAR)
            m.addConstr(bound_new_lya_val == gp.max_(old_lya_val, constant=self._ult_bound))
        else:
            bound_new_lya_val = old_lya_val

        new_lya_val = m.addVar(name="|m(x')|", **self.NNEGVAR)
        m.addConstr(new_lya_val == gp.norm(normed_new_truth, float(self._NORM_ORD)))
        m.addConstr(new_lya_val >= bound_new_lya_val, name="Non-decreasing Error")  # Tracking error is non-decreasing


def test_gem_stanley_min_dist() -> None:
    min_dist = GEMStanleyV2DNonInc(ult_bound=1.0)

    init_state_ub = np.array([np.inf, INIT_Y_LIM, INIT_YAW_LIM])
    init_state_lb = -init_state_ub
    min_dist.set_init_state_bound(lb=init_state_lb, ub=init_state_ub)
    min_dist.set_pre_state_bound(lb=init_state_lb, ub=init_state_ub)

    # print("r =", min_dist.compute_min_dist())
    min_dist.dump_model()


if __name__ == "__main__":
    test_gem_stanley_min_dist()
