import dreal
import numpy as np

from dreal_teacher_base import DRealTeacherBase
from gem_stanley_teacher import CTE_LIM, ANG_LIM, STEERING_LIM, K_P, FORWARD_VEL, CYCLE_TIME, WHEEL_BASE


class GEMStanleyDRealTeacher(DRealTeacherBase):

    def __init__(self, name="gem_stanley", norm_ord=2, delta=0.001) -> None:
        super().__init__(name=name,
                         state_dim=3, perc_dim=2, ctrl_dim=1, norm_ord=norm_ord, delta=delta)

    def _add_system(self) -> None:
        self._set_var_bound(self._old_state, lb=(-np.inf, -CTE_LIM, -ANG_LIM), ub=(np.inf, CTE_LIM, ANG_LIM))
        self._set_var_bound(self._new_state, lb=(-np.inf, -CTE_LIM, -ANG_LIM), ub=(np.inf, CTE_LIM, ANG_LIM))
        self._set_var_bound(self._percept, lb=(-CTE_LIM, -ANG_LIM), ub=(CTE_LIM, ANG_LIM))
        self._set_var_bound(self._control, lb=(-STEERING_LIM,), ub=(STEERING_LIM,))

        # Variable Aliases
        old_x, old_y, old_yaw = self._old_state
        new_x, new_y, new_yaw = self._new_state
        cte, phi = self._percept
        steering, = self._control

        self._not_inv_cons.extend([
            # Control
            steering == dreal.Min(dreal.Max(phi + dreal.atan(cte*(K_P / FORWARD_VEL)), -STEERING_LIM), STEERING_LIM),
            # Dynamics
            new_x == old_x + FORWARD_VEL*CYCLE_TIME*dreal.cos(old_yaw + steering),
            new_y == old_y + FORWARD_VEL*CYCLE_TIME*dreal.sin(old_yaw + steering),
            new_yaw == old_yaw + dreal.sin(steering)*FORWARD_VEL*CYCLE_TIME/WHEEL_BASE,
        ])

    def _add_unsafe(self) -> None:
        # Variable Aliases
        old_x, old_y, old_yaw = self._old_state
        new_x, new_y, new_yaw = self._new_state

        if self._norm_ord == 1:
            old_err = abs(old_y) + abs(old_yaw)
            new_err = abs(new_y) + abs(new_yaw)
        elif self._norm_ord == 2:
            old_err = dreal.sqrt(old_y**2 + old_yaw**2)
            new_err = dreal.sqrt(new_y**2 + new_yaw**2)
        else:
            assert self._norm_ord == "inf"
            old_err = dreal.Max(abs(old_y), abs(old_yaw))
            new_err = dreal.Max(abs(new_y), abs(new_yaw))

        self._not_inv_cons.extend([
            old_err <= new_err
        ])


def test_gem_stanley_dreal_teacher():
    a_mat = np.asfarray([[0, -1, 0],
                         [0, 0, -1]])
    b_vec = np.zeros(2)
    coeff_mat = np.array([
        [-1, -1],
        [-1, 1],
        [1, -1],
        [1, 1],
    ])
    cut_vec = np.array([0.05, 0.05, 0.05, 0.05])

    teacher = GEMStanleyDRealTeacher(norm_ord=1, delta=0.01)
    teacher.set_old_state_bound(
        lb=(-np.inf, 0.5, 0.0),
        ub=(np.inf, 1.2, np.pi/12)
    )
    # teacher.dump_model()
    res = teacher.check((a_mat, b_vec, coeff_mat, cut_vec))
    print(res)


if __name__ == "__main__":
    test_gem_stanley_dreal_teacher()
