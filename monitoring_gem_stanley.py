#!/usr/bin/env python3

import json
from pathlib import Path
import pickle
from typing import List, Tuple

from joblib import Parallel, delayed
import matplotlib.pyplot as plt
import numpy as np
import z3

from gem_stanley_teacher import DTreeGEMStanleyGurobiStabilityTeacher
from agbot_stanley_teacher import DTreeAgBotStanleyGurobiTeacher


def fp_to_real(val: float):
    assert np.isfinite(val)
    return z3.fpToReal(z3.FPVal(val, z3.Float64()))


def build_aap_predicate(
        state_vars: z3.ExprRef,
        perc_vars: z3.ExprRef,
        aap_json) -> z3.BoolRef:
    found_dtree = [dict(part=entry["part"], **entry["result"])
                   for entry in aap_json if entry["status"] == "found"]

    otherwise_list = []  # type: List[z3.BoolRef]
    aap_formula_list = []
    for m in found_dtree:
        assert len(state_vars) == len(m['part'])

        pre_list = []
        for x, (lb, ub) in zip(state_vars, m['part']):
            if np.isfinite(lb):
                pre_list.append(x >= fp_to_real(lb))
            else:
                assert np.isneginf(lb)
            if np.isfinite(ub):
                pre_list.append(x <= fp_to_real(ub))
            else:
                assert np.isposinf(ub)

        dtree = z3.deserialize(m["smtlib"])

        aap_formula_list.append(z3.Implies(z3.And(*pre_list), dtree))
        otherwise_list.append(z3.Not(z3.And(*pre_list)))

    aap_formula_list.append(
        z3.Implies(
            z3.And(*otherwise_list), z3.BoolVal(False)
        )
    )
    return z3.And(aap_formula_list)


def monitoring(
        gt_list: List[Tuple[float, ...]],
        gte_list: List[Tuple[float, ...]],
        gt_var_names: List[str],
        gte_var_names: List[str],
        aap_pred_smtlib: str):
    assert len(gt_list) == len(gte_list)
    gt_vars = z3.Reals(gt_var_names)
    gte_vars = z3.Reals(gte_var_names)

    aap_pred = z3.deserialize(aap_pred_smtlib)

    bool_list = []  # type: List[z3.BoolRef]
    for gt, gte in zip(gt_list, gte_list):
        subs = [(var, fp_to_real(val)) for var, val in zip(gt_vars, gt)] \
            + [(var, fp_to_real(val)) for var, val in zip(gte_vars, gte)]
        bool_list.append(
            z3.simplify(z3.substitute(aap_pred, *subs))
        )
    bool_list = [z3.is_true(b) for b in bool_list]
    return bool_list


def parallel_monitoring(trace_pairs, gt_var_names, gte_var_names, pred: z3.BoolRef, num_jobs: int):
    in_pred_trace_list = Parallel(num_jobs)(
        delayed(monitoring)(
            gt_list, gte_list, gt_var_names, gte_var_names,
            pred.serialize())
        for gt_list, gte_list in trace_pairs)
    num_safe_total_list = [(sum(bool_trace), len(bool_trace)) for bool_trace in in_pred_trace_list]
    in_pred_arr, total_arr = np.array(num_safe_total_list).T
    pass_rate = in_pred_arr / total_arr
    print(f"Min % positive states: {np.min(pass_rate)*100:.2f}%")
    print(f"Avg % positive states: {np.mean(pass_rate)*100:.2f}%")
    print(f"Max % positive states: {np.max(pass_rate)*100:.2f}%")

    return in_pred_trace_list


def check_gt_in_aap(
        aap_pred: z3.BoolRef,
        gt_vars: List[z3.ExprRef],
        gte_vars: List[z3.ExprRef]) -> None:
    """
    Check if the AAP predicate holds when the ground truth equals estimate.
    """
    assert z3.is_and(aap_pred)

    print("="*80)
    vals = gt_vars
    sub_vals = [(gt, val) for gt, val in zip(gt_vars, vals)] + \
        [(gte, val) for gte, val in zip(gte_vars, vals)]
    for i, pred in enumerate(aap_pred.children()):
        gt_in_aap = z3.simplify(z3.substitute(pred, *sub_vals))
        if z3.is_true(gt_in_aap):
            continue
        s = z3.Solver()
        s.add(z3.Not(gt_in_aap))
        res = s.check()
        if res != z3.unsat:
            print(f"Part {i}")
            print(s.model())


def plot_compare_gtdc_aap(
        is_safe_ex,
        gt_vars,
        gte_vars,
        aap_pred: z3.BoolRef,
        gtdc_pred: z3.BoolRef):
    # Plot for single ground truth
    gt_cte, gt_psi = 1.0, 0.01  # buggy in 4d concat domain
    # gt_cte, gt_psi = -0.125, 0.1875
    plt.scatter(gt_cte, gt_psi, s=0.5, c='b')

    NUM_PTS = 400
    cte_grid, psi_grid = np.meshgrid(
        np.linspace(-2, 2, NUM_PTS),
        np.linspace(-np.pi/2, np.pi/2, NUM_PTS)
    )
    cte_arr = cte_grid.ravel()
    psi_arr = psi_grid.ravel()

    # Plot unsafe
    unsafe = np.array([
        not is_safe_ex((2.0, -gt_cte, -gt_psi, x, y)) for x, y in zip(cte_arr, psi_arr)
    ], dtype=int).reshape((NUM_PTS, NUM_PTS))

    plt.imshow(
        unsafe,
        extent=(cte_grid.min(), cte_grid.max(), psi_grid.min(), psi_grid.max()),
        origin="lower", cmap="Reds", alpha=0.3)

    # Plot AAP
    aap_pred = z3.simplify(
        z3.substitute(
            aap_pred,
            (gt_vars[0], fp_to_real(gt_cte)),
            (gt_vars[1], fp_to_real(gt_psi)))
    )
    print("AAP after evaluating GT:", aap_pred)
    aap = []
    for x, y in zip(cte_arr, psi_arr):
        subs = [(var, fp_to_real(val)) for var, val in zip(gte_vars, [x, y])]
        aap.append(
            z3.is_true(z3.simplify(z3.substitute(aap_pred, *subs)))
        )
    aap_arr = np.array(aap).reshape((NUM_PTS, NUM_PTS))
    plt.imshow(
        aap_arr,
        extent=(cte_grid.min(), cte_grid.max(), psi_grid.min(), psi_grid.max()),
        origin="lower", cmap="Purples", alpha=0.3)

    # Plot GTDC
    gtdc_pred = z3.simplify(
        z3.substitute(
            gtdc_pred,
            (gt_vars[0], fp_to_real(gt_cte)),
            (gt_vars[1], fp_to_real(gt_psi)))
    )
    print("GTDC after evaluating GT:", gtdc_pred)
    gtdc = []
    for x, y in zip(cte_arr, psi_arr):
        subs = [(var, fp_to_real(val)) for var, val in zip(gte_vars, [x, y])]
        gtdc.append(
            z3.is_true(z3.simplify(z3.substitute(gtdc_pred, *subs)))
        )
    gtdc_arr = np.array(gtdc).reshape((NUM_PTS, NUM_PTS))
    plt.imshow(
        gtdc_arr,
        extent=(cte_grid.min(), cte_grid.max(), psi_grid.min(), psi_grid.max()),
        origin="lower", cmap="Greens", alpha=0.3)

    plt.tight_layout(pad=1.3)
    plt.xlabel("Estimated d (m)")
    plt.ylabel("Estimated ψ (rad)")
    plt.savefig("test-gtdc-aap.png")


def calc_agree_rates(in_pred_table, in_spec_table):
    # Calculate agreeing rate
    imply_rates = [sum(is_imply_trace)/len(is_imply_trace)
                   for is_imply_trace in np.logical_or(np.logical_not(in_pred_table), in_spec_table)]
    print("="*80)
    print(f"Min % imply: {np.min(imply_rates)*100:.2f}%")
    print(f"Avg % imply: {np.mean(imply_rates)*100:.2f}%")
    print(f"Max % imply: {np.max(imply_rates)*100:.2f}%")

    precision = np.sum(in_pred_table) / np.sum(in_spec_table)
    print(precision.shape)
    print("="*80)
    print(f"Precision: {precision*100:.2f}%")
    return precision


def main_gem_stanley():
    DOM = "concat"
    ULT_BOUND_STR = "0.0"
    NORM_STR = "2"
    ULT_BOUND = float(ULT_BOUND_STR)
    NORM_ORD = int(NORM_STR)

    MAX_VEL = 2.8  # m/s

    NUM_JOBS = 40
    AAP_JSON_FILE = Path("aap.4x10.out.json")
    assert AAP_JSON_FILE.exists()
    GTDC_JSON_PATH = Path(f"out-stability-{DOM}-ult_bound={ULT_BOUND_STR}-norm=L{NORM_STR}")
    GTDC_JSON_FILE = GTDC_JSON_PATH / "out" / "dtree_synth.4x10.out.json"
    assert GTDC_JSON_FILE.exists(), GTDC_JSON_FILE
    TRACE_PKL_FILE = Path("data/monitoring/gem_stanley-uniform-800_traces-400_psicte.pickle")

    with open(TRACE_PKL_FILE, "rb") as pkl:
        trace_arr_pairs = pickle.load(pkl)
    trace_pairs = []
    for gt_trace_arr, gte_trace_arr in trace_arr_pairs:
        # Extract only relevant fields and order the fields correctly
        assert len(gt_trace_arr) == len(gte_trace_arr) + 1
        gt_trace_arr = gt_trace_arr[:-1]
        gt_list = gt_trace_arr[['cte', 'psi']].tolist()
        gte_list = gte_trace_arr[['cte', 'psi']].tolist()
        trace_pairs.append((gt_list, gte_list))

    print("# Traces:", len(trace_pairs))
    print("# States in each trace:", len(trace_pairs[0][1]))

    teacher = DTreeGEMStanleyGurobiStabilityTeacher(norm_ord=NORM_ORD, ultimate_bound=ULT_BOUND)
    def is_safe_ex(ex) -> bool:
        return teacher.is_safe_state(ex, vel=MAX_VEL)

    gt_var_names = ["d", "psi"]
    gte_var_names = ["d_e", "psi_e"]
    gt_vars = z3.Reals(gt_var_names)
    gte_vars = z3.Reals(gte_var_names)

    state_vars = [z3.Real(f"x_{i}") for i in range(3)]
    perc_vars = [z3.Real(f"z_{i}") for i in range(2)]
    # NOTE temporarily convert state/latent variables to gt variables
    subs = [(x, -gt) for x, gt in zip(state_vars[1:], gt_vars)] + \
        [(p, gte) for p, gte in zip(perc_vars, gte_vars)]

    # TODO Merge into parallel monitoring
    print("Monitor with teacher defined predicate")
    in_spec_trace_list = [
        [is_safe_ex((0.0,) + tuple(-v for v in gt) + gte)
         for gt, gte in zip(gt_list, gte_list)]
        for gt_list, gte_list in trace_pairs
    ]
    in_spec_table = np.array(in_spec_trace_list, dtype=bool)
    # num_safe_total_list = [(sum(bool_trace), len(bool_trace)) for bool_trace in in_spec_trace_list]
    # in_pred_arr, total_arr = np.array(num_safe_total_list).T
    # pass_rate = in_pred_arr / total_arr
    # print(f"Min % positive states: {np.min(pass_rate)*100:.2f}%")
    # print(f"Avg % positive states: {np.mean(pass_rate)*100:.2f}%")
    # print(f"Max % positive states: {np.max(pass_rate)*100:.2f}%")

    # print("="*80)
    # print("Monitor with AAP predicate")
    # with open(AAP_JSON_FILE) as json_fp:
    #     aap_data = json.load(json_fp)
    # aap_pred = build_aap_predicate(state_vars, perc_vars, aap_data)
    # aap_pred = z3.substitute(aap_pred, *subs)
    # aap_pred = z3.simplify(aap_pred)
    # in_aap_trace_list = parallel_monitoring(
    #     trace_pairs, gt_var_names, gte_var_names, aap_pred, NUM_JOBS
    # )
    # in_aap_table = np.array(in_aap_trace_list, dtype=bool)
    # calc_agree_rates(in_pred_table=in_aap_table, in_spec_table=in_spec_table)

    print("="*80)
    print("Monitor with GTDC predicate")
    with open(GTDC_JSON_FILE) as json_fp:
        gtdc_data = json.load(json_fp)
    gtdc_pred = build_aap_predicate(state_vars, perc_vars, gtdc_data)
    gtdc_pred = z3.substitute(gtdc_pred, *subs)
    gtdc_pred = z3.simplify(gtdc_pred)
    in_gtdc_trace_list = parallel_monitoring(
        trace_pairs, gt_var_names, gte_var_names, gtdc_pred, NUM_JOBS
    )
    in_gtdc_table = np.array(in_gtdc_trace_list, dtype=bool)
    calc_agree_rates(in_pred_table=in_gtdc_table, in_spec_table=in_spec_table)

    return
    print("="*80)
    print("Plot unsafe, AAP, and GTDC predicates")
    plot_compare_gtdc_aap(
        is_safe_ex=is_safe_ex,
        gt_vars=gt_vars, gte_vars=gte_vars,
        aap_pred=aap_pred, gtdc_pred=gtdc_pred
    )


def main_agbot_stanley():
    DOM = "diff"
    ULT_BOUND_STR = "1.0"
    NORM_STR = "2"
    ULT_BOUND = float(ULT_BOUND_STR)
    NORM_ORD = int(NORM_STR)

    NUM_JOBS = 40
    GTDC_JSON_PATH = Path(f"out-agbot-stability-{DOM}-ult_bound={ULT_BOUND_STR}")
    GTDC_JSON_FILE = GTDC_JSON_PATH / "out" / "dtree_synth.5x5.out.json"
    TRACE_PKL_FILE = Path("data/monitoring/agbot_stanley-uniform-200_traces-200_psicte.pickle")

    with open(TRACE_PKL_FILE, "rb") as pkl:
        trace_arr_pairs = pickle.load(pkl)
    trace_pairs = []
    for gt_trace_arr, gte_trace_arr in trace_arr_pairs:
        # Extract only relevant fields and order the fields correctly
        assert len(gt_trace_arr) == len(gte_trace_arr) + 1
        gt_trace_arr = gt_trace_arr[:-1]
        gt_list = gt_trace_arr[['cte', 'psi']].tolist()
        gte_list = gte_trace_arr[['cte', 'psi']].tolist()
        trace_pairs.append((gt_list, gte_list))

    print("# Traces:", len(trace_pairs))
    print("# States in each trace:", len(trace_pairs[0][1]))

    teacher = DTreeAgBotStanleyGurobiTeacher(norm_ord=NORM_ORD, ultimate_bound=ULT_BOUND)

    gt_var_names = ["d", "psi"]
    gte_var_names = ["d_e", "psi_e"]
    gt_vars = z3.Reals(gt_var_names)
    gte_vars = z3.Reals(gte_var_names)

    state_vars = [z3.Real(f"x_{i}") for i in range(3)]
    perc_vars = [z3.Real(f"z_{i}") for i in range(2)]
    # NOTE temporarily convert state/latent variables to gt variables
    subs = [(x, -gt) for x, gt in zip(state_vars[1:], gt_vars)] + \
        [(p, gte) for p, gte in zip(perc_vars, gte_vars)]

    # TODO Merge into parallel monitoring
    print("Monitor with teacher defined predicate")
    in_spec_trace_list = [
        [teacher.is_safe_state((0.0,) + tuple(-v for v in gt) + gte)
         for gt, gte in zip(gt_list, gte_list)]
        for gt_list, gte_list in trace_pairs
    ]
    in_spec_table = np.array(in_spec_trace_list, dtype=bool)
    num_safe_total_list = [(sum(bool_trace), len(bool_trace)) for bool_trace in in_spec_trace_list]
    in_pred_arr, total_arr = np.array(num_safe_total_list).T
    pass_rate = in_pred_arr / total_arr
    print(f"Min % positive states: {np.min(pass_rate)*100:.2f}%")
    print(f"Avg % positive states: {np.mean(pass_rate)*100:.2f}%")
    print(f"Max % positive states: {np.max(pass_rate)*100:.2f}%")


    print("="*80)
    print("Monitor with GTDC predicate")
    with open(GTDC_JSON_FILE) as json_fp:
        gtdc_data = json.load(json_fp)
    gtdc_pred = build_aap_predicate(state_vars, perc_vars, gtdc_data)
    gtdc_pred = z3.substitute(gtdc_pred, *subs)
    gtdc_pred = z3.simplify(gtdc_pred)
    in_gtdc_trace_list = parallel_monitoring(
        trace_pairs, gt_var_names, gte_var_names, gtdc_pred, NUM_JOBS
    )
    in_gtdc_table = np.array(in_gtdc_trace_list, dtype=bool)
    calc_agree_rates(in_pred_table=in_gtdc_table, in_spec_table=in_spec_table)

if __name__ == "__main__":
    main_gem_stanley()
    # main_agbot_stanley()
