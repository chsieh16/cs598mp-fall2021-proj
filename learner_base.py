import abc

import z3


class LearnerBase(abc.ABC):
    def __init__(self) -> None:
        pass

    @abc.abstractmethod
    def set_grammar(self, grammar) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def add_positive_examples(self, *args) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def add_negative_examples(self, *args) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def add_implication_examples(self, *args) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def learn(self):
        raise NotImplementedError


class Z3LearnerBase(LearnerBase):
    def __init__(self, state_dim: int, perc_dim: int,
                 timeout: int = 10000) -> None:
        super().__init__()
        z3.set_param("timeout", timeout,
                     "solver.timeout", timeout,
                     "model.completion", True,
                     "smt.logic", "QF_LRA")

        self._solver = z3.AndThen('qfnra-nlsat', 'smt').solver()
        # self._solver.set('ctrl_c', True)
        real_var_vec = z3.RealVarVector(state_dim + perc_dim,
                                        ctx=self._solver.ctx)
        self._state_vars = real_var_vec[0:state_dim]
        self._percept_vars = real_var_vec[state_dim:state_dim+perc_dim]

    @property
    def state_dim(self) -> int:
        return len(self._state_vars)

    @property
    def perc_dim(self) -> int:
        return len(self._percept_vars)
