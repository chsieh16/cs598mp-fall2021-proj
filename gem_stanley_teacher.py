import gurobipy as gp
import numpy as np
import sympy
import z3

from teacher_base import SymPyTeacherBase
from dtree_teacher_base import DTreeGurobiTeacherBase

WHEEL_BASE = 1.75  # m

K_P = 0.45

CYCLE_TIME = 0.05  # second
FORWARD_VEL_MIN = 2.5  # m/s
FORWARD_VEL_MAX = 3.0  # m/s
STEERING_LIM = 0.61  # rad

# Limits on unconstrained variables to avoid overflow and angle normalization
ANG_LIM = np.pi / 2  # radian
CTE_LIM = 2.0  # meter

# Bounds for intermediate variables.
# These are not necessary but can improve efficiency
K_CTE_V_LIM = K_P * 20 * CTE_LIM / FORWARD_VEL_MIN
ATAN_K_CTE_V_LIM = np.arctan(K_CTE_V_LIM)
RAW_ANG_ERR_LIM = (ANG_LIM + ATAN_K_CTE_V_LIM)


class DTreeGEMStanleyGurobiTeacherBase(DTreeGurobiTeacherBase):
    # Ideal perception as a linear transform from state to ground truth percept
    PERC_GT = np.array([[0., -1., 0.],
                        [0., 0., -1.]], float)

    def __init__(self, name="gem_stanley", norm_ord=2) -> None:
        super().__init__(name=name,
                         state_dim=3, perc_dim=2, ctrl_dim=1, norm_ord=norm_ord)

    def _add_system(self) -> None:
        # Bounds on all domains
        self._old_state.lb = (-np.inf, -CTE_LIM, -ANG_LIM)
        self._old_state.ub = (np.inf, CTE_LIM, ANG_LIM)
        self._percept.lb = (-CTE_LIM, -ANG_LIM)
        self._percept.ub = (CTE_LIM, ANG_LIM)
        self._control.lb = (-STEERING_LIM,)
        self._control.ub = (STEERING_LIM,)

        # Variable Aliases
        m = self._gp_model
        old_x, old_y, old_yaw = self._old_state.tolist()
        new_x, new_y, new_yaw = self._new_state.tolist()
        cte, psi = self._percept.tolist()
        steering, = self._control.tolist()

        vel = m.addVar(name="Vf", lb=FORWARD_VEL_MIN, ub=FORWARD_VEL_MAX)

        # Controller
        K_cte_V = m.addVar(name="K*d/Vf", lb=-K_CTE_V_LIM, ub=K_CTE_V_LIM)
        m.addConstr(cte == K_cte_V*vel/K_P)
        atan_K_cte_V = m.addVar(name="atan(K*d/Vf)",
                                lb=-ATAN_K_CTE_V_LIM, ub=ATAN_K_CTE_V_LIM)
        m.addGenConstrTan(xvar=atan_K_cte_V, yvar=K_cte_V)
        # Clip steering angle
        error = m.addVar(name="(φ+atan(K*d/Vf))",
                         lb=-RAW_ANG_ERR_LIM, ub=RAW_ANG_ERR_LIM)
        m.addConstr(error == psi + atan_K_cte_V)

        xpts = [-RAW_ANG_ERR_LIM, -STEERING_LIM, STEERING_LIM, RAW_ANG_ERR_LIM]
        ypts = [-STEERING_LIM, -STEERING_LIM, STEERING_LIM, STEERING_LIM]
        m.addGenConstrPWL(name="clip", xvar=error, yvar=steering,
                          xpts=xpts, ypts=ypts)

        # Dynamics
        yaw_steer = m.addVar(name="θ+δ",
                             lb=-(ANG_LIM + STEERING_LIM),
                             ub=(ANG_LIM + STEERING_LIM))
        m.addConstr(yaw_steer == (old_yaw + steering))

        cos_yaw_steer = m.addVar(name="cos(θ+δ)", **self.TRIGVAR)
        m.addGenConstrCos(yaw_steer, cos_yaw_steer)
        sin_yaw_steer = m.addVar(name="sin(θ+δ)", **self.TRIGVAR)
        m.addGenConstrSin(yaw_steer, sin_yaw_steer)
        sin_steer = m.addVar(name="sinδ", **self.TRIGVAR)
        m.addGenConstrSin(steering, sin_steer)

        m.addConstr(new_x == old_x + vel*CYCLE_TIME*cos_yaw_steer)
        m.addConstr(new_y == old_y + vel*CYCLE_TIME*sin_yaw_steer)
        m.addConstr(new_yaw ==
                    old_yaw + sin_steer*vel*CYCLE_TIME/WHEEL_BASE)
        m.update()

    def _add_objective(self) -> None:
        # Variable Aliases
        m = self._gp_model
        x = self._old_state
        z = self._percept
        z_diff = self._percept_diff

        # Add objective
        norm_var = m.addVar(name="|z-m(x)|", **self.NNEGVAR)
        m.addConstr(z_diff == z - self.PERC_GT @ x)
        m.addConstr(norm_var == gp.norm(z_diff, float(self._norm_ord)))

        m.setObjective(norm_var, gp.GRB.MINIMIZE)
        m.update()


class DTreeGEMStanleyGurobiStabilityTeacher(DTreeGEMStanleyGurobiTeacherBase):
    def __init__(self, name="gem_stanley_stability", norm_ord=2, ultimate_bound: float = 0.0) -> None:
        assert ultimate_bound >= 0.0
        self._ultimate_bound = ultimate_bound
        super().__init__(name=name, norm_ord=norm_ord)

    def is_safe_state(self, ex, vel=None) -> bool:
        assert len(ex) == self.state_dim + self.perc_dim

        if vel is None:
            vel = FORWARD_VEL_MAX

        def g(perc):
            cte, psi = perc
            error = psi + np.arctan(K_P*cte/vel)
            steer = np.clip(error, -STEERING_LIM, STEERING_LIM)
            return (steer,)

        def f(state, ctrl):
            x, y, theta = state
            steer, = ctrl
            new_x = x + vel*np.cos(theta+steer)*CYCLE_TIME
            new_y = y + vel*np.sin(theta+steer)*CYCLE_TIME
            new_theta = theta + vel*np.sin(steer)/WHEEL_BASE*CYCLE_TIME
            return new_x, new_y, new_theta

        def m_star(state):
            return self.PERC_GT @ state

        def v(gt_perc) -> float:
            return float(np.linalg.norm(gt_perc, ord=float(self._norm_ord)))

        def spec(state, perc) -> bool:
            v_old = v(m_star(state))
            v_new = v(m_star(f(state, g(perc))))
            return v_new <= max(v_old, self._ultimate_bound)
        return spec(ex[0:self.state_dim],
                    ex[self.state_dim: self.state_dim+self.perc_dim])

    def _add_unsafe(self) -> None:
        assert self.PERC_GT.shape == (self.perc_dim, self.state_dim)
        # Variable Aliases
        m = self._gp_model

        # Add nonincreasing constraints
        old_truth = m.addMVar(shape=(self.perc_dim,), name="m(x)", **self.FREEVAR)
        m.addConstr(old_truth == self.PERC_GT @ self._old_state)
        old_lya_val = m.addVar(name="|m(x)|", **self.NNEGVAR)
        m.addConstr(old_lya_val == gp.norm(old_truth, float(self._norm_ord)))
        # Add invariant bound
        # m.addConstr(old_lya_val <= self._inv_bound)
        # Add ultimate bound
        if self._ultimate_bound > 0:
            bound_new_lya_val = m.addVar(name=f"max(|m(x)|,{self._ultimate_bound})",
                                         **self.NNEGVAR)
            m.addConstr(bound_new_lya_val == gp.max_(old_lya_val, constant=self._ultimate_bound))
        else:
            bound_new_lya_val = old_lya_val

        new_truth = m.addMVar(shape=(self.perc_dim,), name="m(x')", **self.FREEVAR)
        m.addConstr(new_truth == self.PERC_GT @ self._new_state)
        new_lya_val = m.addVar(name="|m(x')|", **self.NNEGVAR)
        m.addConstr(new_lya_val == gp.norm(new_truth, float(self._norm_ord)))

        # Negation of stability spec
        m.addConstr(new_lya_val >= bound_new_lya_val, name="Increasing Error")  # Tracking error is increasing
        m.update()


class DTreeGEMStanleyGurobiBarrierTeacher(DTreeGEMStanleyGurobiTeacherBase):
    def __init__(self, name="gem_stanley_barrier", norm_ord=2, inv_bound: float = 1.25) -> None:
        assert inv_bound > 0.0
        self._inv_bound = inv_bound

        super().__init__(name=name, norm_ord=norm_ord)

    def is_safe_state(self, ex, vel=FORWARD_VEL_MAX) -> bool:
        assert len(ex) == self.state_dim + self.perc_dim

        def g(perc):
            cte, psi = perc
            error = psi + np.arctan(K_P*cte/vel)
            steer = np.clip(error, -STEERING_LIM, STEERING_LIM)
            return (steer,)

        def f(state, ctrl):
            x, y, theta = state
            steer, = ctrl
            new_x = x + vel*np.cos(theta+steer)*CYCLE_TIME
            new_y = y + vel*np.sin(theta+steer)*CYCLE_TIME
            new_theta = theta + vel*np.sin(steer)/WHEEL_BASE*CYCLE_TIME
            return new_x, new_y, new_theta

        def m_star(state):
            return self.PERC_GT @ state

        def v(gt_perc) -> float:
            return float(np.linalg.norm(gt_perc, ord=float(self._norm_ord)))

        def spec(state, perc) -> bool:
            v_old = v(m_star(state))
            v_new = v(m_star(f(state, g(perc))))
            # (v(x) <= rho) ==> (v(x') <= rho)
            return (not v_old <= self._inv_bound) or v_new <= self._inv_bound
        return spec(ex[0:self.state_dim],
                    ex[self.state_dim: self.state_dim+self.perc_dim])

    def _add_unsafe(self) -> None:
        assert self.PERC_GT.shape == (self.perc_dim, self.state_dim)
        # Variable Aliases
        m = self._gp_model

        # Add barrier bound on old state
        old_truth = m.addMVar(shape=(self.perc_dim,), name="m(x)", **self.FREEVAR)
        m.addConstr(old_truth == self.PERC_GT @ self._old_state)
        old_lya_val = m.addVar(name="|m(x)|", **self.NNEGVAR)
        m.addConstr(old_lya_val == gp.norm(old_truth, float(self._norm_ord)))
        m.addConstr(old_lya_val <= self._inv_bound)

        # Add negation of barrier on new state
        new_truth = m.addMVar(shape=(self.perc_dim,), name="m(x')", **self.FREEVAR)
        m.addConstr(new_truth == self.PERC_GT @ self._new_state)
        new_lya_val = m.addVar(name="|m(x')|", **self.NNEGVAR)
        m.addConstr(new_lya_val == gp.norm(new_truth, float(self._norm_ord)))
        m.addConstr(new_lya_val >= self._inv_bound)
        m.update()


class GEMStanleySymPyTeacher(SymPyTeacherBase):
    CTE_LIM = sympy.Rational("2.0")
    ANG_LIM = sympy.pi / 2
    STEERING_LIM = sympy.Rational("0.61")
    K_P = sympy.Rational("0.45")
    FORWARD_VEL = sympy.Rational("2.8")
    CYCLE_TIME = sympy.Rational("0.05")
    WHEEL_BASE = sympy.Rational("1.75")

    def __init__(self, name="gem_stanley", norm_ord=2) -> None:
        super().__init__(name=name,
                         state_dim=3, perc_dim=2, ctrl_dim=1, norm_ord=norm_ord)

    def _add_system(self) -> None:
        self._set_var_bound(self._old_state,
                            lb=(-sympy.oo, -self.CTE_LIM, -self.ANG_LIM),
                            ub=(sympy.oo, self.CTE_LIM, self.ANG_LIM))
        self._set_var_bound(self._new_state,
                            lb=(-sympy.oo, -self.CTE_LIM, -self.ANG_LIM),
                            ub=(sympy.oo, self.CTE_LIM, self.ANG_LIM))
        self._set_var_bound(self._percept,
                            lb=(-self.CTE_LIM, -self.ANG_LIM),
                            ub=(self.CTE_LIM, self.ANG_LIM))
        self._set_var_bound(self._control,
                            lb=(-self.STEERING_LIM,),
                            ub=(self.STEERING_LIM,))

        # Variable Aliases
        old_x, old_y, old_yaw = self._old_state
        new_x, new_y, new_yaw = self._new_state
        cte, psi = self._percept
        steering, = self._control

        err = psi + sympy.atan2(cte*self.K_P, self.FORWARD_VEL)
        clipped_err = sympy.Piecewise(
            (self.STEERING_LIM, err > self.STEERING_LIM),
            (-self.STEERING_LIM, err < -self.STEERING_LIM),
            (err, True)
        )

        self._not_inv_cons.extend([
            # Control
            sympy.Eq(steering, clipped_err),
            # Dynamics
            sympy.Eq(new_x, old_x + self.FORWARD_VEL*self.CYCLE_TIME*sympy.cos(old_yaw + steering)),
            sympy.Eq(new_y, old_y + self.FORWARD_VEL*self.CYCLE_TIME*sympy.sin(old_yaw + steering)),
            sympy.Eq(new_yaw, old_yaw + sympy.sin(steering)*self.FORWARD_VEL*self.CYCLE_TIME/self.WHEEL_BASE),
        ])

    def _add_unsafe(self) -> None:
        # Variable Aliases
        old_x, old_y, old_yaw = self._old_state
        new_x, new_y, new_yaw = self._new_state

        norm_ord = sympy.oo if self._norm_ord == "inf" else self._norm_ord
        old_err = sympy.Matrix([old_y, old_yaw]).norm(ord=norm_ord)
        new_err = sympy.Matrix([new_y, new_yaw]).norm(ord=norm_ord)
        self._not_inv_cons.extend([
            old_err <= new_err
        ])


def test_gem_stanley_sympy_teacher():
    teacher = GEMStanleySymPyTeacher(norm_ord=2)
    teacher.set_old_state_bound(
        lb=(-sympy.oo, sympy.Rational("0.5"), 0.0),
        ub=(sympy.oo, sympy.Rational("1.2"), sympy.pi/12)
    )
    teacher.dump_system_encoding()


def test_gem_stanley_gurobi_teacher():
    teacher = DTreeGEMStanleyGurobiBarrierTeacher(inv_bound=1.25)
    teacher.set_old_state_bound(
        lb=(-np.inf, 0.5, 0.0625),
        ub=(np.inf, 1.0, 0.25)
    )
    teacher.dump_system_encoding()


def test_dtree_gem_stanley_gurobi_teacher():
    x_0, x_1 = z3.Reals("x_0 x_1")
    candidate = z3.If(x_0 >= 0.2,
                      z3.If(x_1 <= 1, z3.BoolVal(True), 0.5*x_0 + x_1 <= 3),
                      0.5*x_0 + x_1 > 3)

    teacher = DTreeGEMStanleyGurobiBarrierTeacher(norm_ord=2)
    print(teacher.check(candidate))
    print(teacher.model())


if __name__ == "__main__":
    test_gem_stanley_gurobi_teacher()
    # test_gem_stanley_sympy_teacher()
    test_dtree_gem_stanley_gurobi_teacher()
