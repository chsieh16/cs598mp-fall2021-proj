from __future__ import print_function
import pickle
from typing import List, Tuple

import numpy as np
import z3
from z3 import *

from sygus_learner import SygusLearner as Learner
from gem_stanley_teacher import GEMStanleyGurobiTeacher as Teacher



# x, y = Ints('x y')
# fml = x + x + y > 2
# seen = {}
# for e in visitor(fml, seen):
#     if is_const(e) and e.decl().kind() == Z3_OP_UNINTERPRETED:
#         print("Variable", e)
#     else:
#         print(e)


def test_synth_region():
    pickle_file_io = open("data/collect_images_2021-11-22-17-59-46.cs598.filtered.pickle", "rb")
    pkl_data = pickle.load(pickle_file_io)

    truth_samples_seq = pkl_data["truth_samples"]

    i_th = 0  # select only the i-th partition
    truth_samples_seq = truth_samples_seq[i_th:i_th+1]
    print("Representative point in partition:", truth_samples_seq[0][0])

    truth_samples_seq = [(t, [s for s in raw_samples if not any(np.isnan(s))])
                         for t, raw_samples in truth_samples_seq]
    # Chiao: Read in positive samples
    positive_examples: List[Tuple[float, ...]] = [
        s for _, samples in truth_samples_seq for s in samples
    ]

    ex_dim = len(positive_examples[0])
    positive_examples = positive_examples[:20:]
    print("#examples: %d" % len(positive_examples))
    print("Dimension of each example: %d" % ex_dim)
    assert all(len(ex) == ex_dim and not any(np.isnan(ex))
               for ex in positive_examples)

    teacher = Teacher()
    assert teacher.state_dim + teacher.perc_dim == ex_dim
    #  0.0 <= x <= 30.0 and -1.0 <= y <= 0.9 and 0.2 <= theta <= 0.22
    teacher.set_old_state_bound(lb=[0.0, -1.0, 0.2], ub=[30.0, -0.9, 0.22])

    synth_region(positive_examples, teacher, num_max_iterations=50)

    #Gurobi encoding
    # 0 is diamond
    # 1 s circle
    # 2 is squares

#TODO: Angello consider a returning a tuple instead of just candidates
def synth_region(positive_examples, teacher, num_max_iterations: int = 10):
    learner = Learner(state_dim=teacher.state_dim,
                      perc_dim=teacher.perc_dim, timeout=20000)

    learner.add_positive_examples(*positive_examples)
    past_candidate_list = []
    #candidate= (0, np.zeros((2,3)), np.zeros((2)), 5.0)
    #print(candidate)
    # print(teacher.check(candidate))

    # print(teacher.model())
    #return
    for k in range(num_max_iterations):
        print(f"Iteration {k}:", sep='')
        print("learning ....")
        candidate = learner.learn()
        print("done learning")

        if candidate is None:  # learning FAILED
            print("Learning Failed.")
            return
        print(f"candidate: {candidate}")
        past_candidate_list.append(candidate)
        # QUERYING TEACHER IF THERE ARE NEGATIVE EXAMPLES
        result = teacher.check(candidate)
        print(result)
        if result == z3.sat:
            m = teacher.model()
            assert len(m) > 0
            # TODO check if negative example state is spurious or true courterexample
            print(f"negative examples: {m}")
            learner.add_negative_examples(*m)

        elif result == z3.unsat:
            print("we are done!")
            return past_candidate_list
        else:
            print("Reason Unknown", teacher.reason_unknown())
            return past_candidate_list

    print("Reached max iteration %d." % num_max_iterations)
    return []


def test_parse_sygus_output():
    # Angello: create learner and add constraints to grammar
    pass
    



if __name__ == "__main__":
    #test_parse_sygus_output()
    test_synth_region()
